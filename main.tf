# See:
#   * https://www.terraform.io/docs/providers/aws/r/iam_user.html
#   * https://www.terraform.io/docs/providers/aws/r/iam_access_key.html
#   * https://www.terraform.io/docs/providers/aws/r/iam_user_login_profile.html

locals {
  enabled = var.iam_user_module_enabled ? true : false

  tags = merge(
    var.iam_user_tags,
    {
      Name      = format("%s", var.iam_user_name)
      terraform = true
    },
  )
}


# Workaround to be able to remove user in case when additional policy were attached outside this module.
resource "null_resource" "detach_user_policies" {
  # The null_resource resource implements the standard resource lifecycle but takes no further action.
  count = local.enabled ? 1 : 0

  depends_on = [
    aws_iam_user.this
  ]

  # The triggers argument allows specifying an arbitrary set of values that, when changed, will cause the resource
  # to be replaced.
  triggers = {
    iam_user_name = var.iam_user_name
  }

  provisioner "local-exec" {
    # If when = "destroy" is specified, the provisioner will run when the resource it is defined within is destroyed.
    when = destroy

    # (Required) This is the command to execute. It can be provided as a relative path to the current working
    # directory or as an absolute path. It is evaluated in a shell, and can use environment variables or
    # Terraform variables.
    command = <<-COMMAND
        aws iam list-attached-user-policies \
            --user-name ${self.triggers.iam_user_name} \
            --query 'AttachedPolicies[*].PolicyArn | join(`"\n"`, @)' --output text \
          | xargs -t -I {USER_POLICY_ARN} aws iam detach-user-policy \
                                              --user-name ${self.triggers.iam_user_name} \
                                              --policy-arn {USER_POLICY_ARN} \
        && sleep 5;
    COMMAND
    # (Optional) block of key value pairs representing the environment of the executed command.
    # Inherits the current process environment.
    environment = {
      # since IAM is global AWS service it isn't possible to stick with specific region, so we can safely
      # hard-code 'us-east-1' as default mostly everywhere.
      AWS_DEFAULT_REGION = "us-east-1"
      AWS_REGION         = "us-east-1"
    }

    # By default, provisioners that fail will also cause the Terraform apply itself to fail. The `on_failure` setting
    # can be used to change this.
    on_failure = continue
  }
}

resource "aws_iam_user" "this" {
  # Provides an IAM user.
  count = local.enabled ? 1 : 0

  # (Required) The user's name.
  name = var.iam_user_name
  # Key-value mapping of tags for the IAM user.
  tags = local.tags

  # (Optional) When destroying this user, destroy even if it has non-Terraform-managed IAM access keys, login profile
  # or MFA devices. Without force_destroy a user with non-Terraform-managed access keys and login profile will fail
  # to be destroyed.

  # NOTE: If policies are attached to the user via the aws_iam_policy_attachment resource and you are modifying
  # the user name or path, the `force_destroy` argument must be set to "true" and applied before attempting
  # the operation otherwise you will encounter a "DeleteConflict" error.
  # The `aws_iam_user_policy_attachment` resource (recommended) does not have this requirement.
  force_destroy = var.iam_user_force_destroy
}

resource "aws_iam_access_key" "this" {
  # Provides an IAM access key. This is a set of credentials that allow API requests to be made as an IAM user.
  count = local.enabled ? 1 : 0

  # (Required) The IAM user to associate with this access key.
  user = join("", aws_iam_user.this.*.name)
  # (Optional) Either a base-64 encoded PGP public key, or a keybase username
  # in the form "keybase:some_person_that_exists".
  pgp_key = var.iam_user_secret_access_pgp_key
  # (Optional) The access key status to apply.
  status = var.iam_user_access_key_status
}

resource "aws_iam_user_login_profile" "this" {
  # Manages an IAM User Login Profile with limited support for password creation during Terraform resource creation.
  # Uses PGP to encrypt the password for safe transport to the user. PGP keys can be obtained from Keybase.

  # To reset an IAM User login password via Terraform, you can use the `terraform taint` command or change any of the
  # arguments.
  count = (local.enabled && var.iam_user_password_enabled) ? 1 : 0

  # (Required) The IAM user's name.
  user = join("", aws_iam_user.this.*.name)
  # (Required) Either a base-64 encoded PGP public key, or a keybase username in the form "keybase:username". Only
  # applies on resource creation. Drift detection is not possible with this argument
  pgp_key = var.iam_user_password_pgp_key
  # (Optional, default 20) The length of the generated password on resource creation. Only applies on resource
  # creation. Drift detection is not possible with this argument.
  password_length = var.iam_user_password_length
  # (Optional, default "true") Whether the user should be forced to reset the generated password on resource creation.
  # Only applies on resource creation. Drift detection is not possible with this argument.
  password_reset_required = var.iam_user_password_reset_required
}

# ------------------------------------------
# Directly assign an IAM policy to the user
# ------------------------------------------
resource "aws_iam_user_policy" "this" {
  # Provides an IAM policy attached to a user.
  count = (local.enabled && length(var.iam_user_policy_document) > 0) ? 1 : 0

  # (Required) The policy document. This is a JSON formatted string.
  policy = var.iam_user_policy_document
  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-", var.iam_user_name)
  # (Required) IAM user to which to attach this policy.
  user = join("", aws_iam_user.this.*.name)
}

# ----------------------------------------------------------------
# Attach managed IAM policies to user's account (recommended way)
# ----------------------------------------------------------------
# NOTE: The usage of this resource conflicts with the `aws_iam_policy_attachment` resource and will permanently show
#       a difference if both are defined.
resource "aws_iam_user_policy_attachment" "this" {
  # Attaches a Managed IAM Policy to an IAM user.
  count = local.enabled ? length(var.iam_user_policies_attachments) : 0

  # (Required) - The user the policy should be applied to
  user = join("", aws_iam_user.this.*.name)
  # (Required) - The ARN of the policy you want to apply
  policy_arn = var.iam_user_policies_attachments[count.index]
}



# -----------------------------------------------
# Manage user group membership (recommended way)
# -----------------------------------------------
# To exclusively manage the users in a group, see the `aws_iam_group_membership` resource.
resource "aws_iam_user_group_membership" "this" {
  # Provides a resource for adding an IAM User to IAM Groups. This resource can be used multiple times with the same
  # user for non-overlapping groups.
  count = (local.enabled && length(var.iam_user_groups) > 0) ? 1 : 0

  # (Required) The name of the IAM User to add to groups
  user = join("", aws_iam_user.this.*.name)
  # (Required) A list of IAM Groups to add the user to
  groups = var.iam_user_groups
}
