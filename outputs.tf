output "user_name" {
  description = "The user's name."
  value       = join("", aws_iam_user.this.*.name)
}
output "user_arn" {
  description = "The ARN assigned by AWS for this user."
  value       = join("", aws_iam_user.this.*.arn)
}
output "user_unique_id" {
  description = "The unique ID assigned by AWS."
  value       = join("", aws_iam_user.this.*.unique_id)
}

output "access_key_id" {
  description = "The access key ID."
  value       = join("", aws_iam_access_key.this.*.id)
}
output "secret_access_key" {
  # Note that this will be written to the state file. Please supply a pgp_key instead, which will prevent the secret
  # from being stored in plain text
  description = "The secret access key."
  value       = join("", length(var.iam_user_secret_access_pgp_key) > 0 ? aws_iam_access_key.this.*.encrypted_secret : aws_iam_access_key.this.*.secret)
}
locals {
  secret_access_key_decrypt_command = length(var.iam_user_secret_access_pgp_key) == 0 ? "" : format(
    "echo '%s' | base64 -d | %s",
    join("", aws_iam_access_key.this.*.encrypted_secret),
    replace(var.iam_user_secret_access_pgp_key, "keybase:", "") == var.iam_user_secret_access_pgp_key ? "gpg --batch --no-tty --yes --passphrase=$GPG_PASSPHRASE --pinentry-mode loopback --decrypt --always-trust" : "keybase pgp decrypt",
  )
}
output "secret_access_key_decrypt_command" {
  description = "Execute this command to decrypt the encrypted secret key."
  value       = length(var.iam_user_secret_access_pgp_key) > 0 ? local.secret_access_key_decrypt_command : ""
}

output "ses_smtp_password" {
  description = "The secret access key converted into an SES SMTP password."
  value       = join("", aws_iam_access_key.this.*.ses_smtp_password_v4)
}


output "console_password" {
  # Only available if password was handled on Terraform resource creation, not import.
  description = "The encrypted password, base64 encoded."
  value       = join("", aws_iam_user_login_profile.this.*.encrypted_password)
}
locals {
  console_password_decrypt_command = format(
    "echo '%s' | base64 -d | %s",
    join("", aws_iam_user_login_profile.this.*.encrypted_password),
    replace(var.iam_user_password_pgp_key, "keybase:", "") == var.iam_user_password_pgp_key ? "gpg --batch --no-tty --yes --passphrase=$GPG_PASSPHRASE --pinentry-mode loopback --decrypt --always-trust" : "keybase pgp decrypt",
  )
}
output "console_password_decrypt_command" {
  description = "Execute this command to decrypt the encrypted console password."
  value       = local.console_password_decrypt_command
}

output "user_policies" {
  description = "The list of IAM Policies attached to the user."
  value       = flatten(aws_iam_user_policy_attachment.this.*.policy_arn)
}
output "user_groups" {
  description = "The list of IAM Groups the user belongs to."
  value       = flatten(aws_iam_user_group_membership.this.*.groups)
}
