aws-iam-user
============

Creates an IAM user account.

## Usage

```hcl-terraform
module "aws-iam-user" {
  source                = "git::https://gitlab.com/codeassist/terragrunt/modules.git//aws-iam-user?ref=<tag>"

# TODO
```

- By default AWS_ACCESS_SECRET_KEY is not shown within the CLI run. To get a hidden data:
    - first (mandatory!) run `terraform apply`...
    - and only then via `teraform output <sensitive_parameter>` it will be possible to get the encrypted data.
- The encrypted password may be decrypted using the command line:
    * in case of using `keybase`:
    ```bash
    terraform output console_password | base64 -d | keybase pgp decrypt
    ```

    * in case of `GPG`:
    ```bash
    # 1. import public part of GPG key (relative path assumes you are in the module root)
    gpg --batch --import gpg/pub.key
    # gpg: key 9734C33AB24712A8: "ITSupportMe, LLC <support@itsm.by>" imported
    # gpg: Total number processed: 1
    # gpg:               imported: 1

    # 2. import private part, enter the password when requested (relative path assumes you are in the module root)
    gpg --batch --import gpg/sec.key
    # gpg: key 9734C33AB24712A8: "ITSupportMe, LLC <support@itsm.by>" secret key imported
    # gpg: Total number processed: 1
    # gpg:       secret keys read: 1
    # gpg:   secret keys imported: 1

    # 3. since private key is protected with the passphrase, set environment variable $GPG_PASSPHRASE
    export GPG_PASSPHRASE=<passphrase here>

    # 4. decrypt the necessary data (this command can be used locally and within CI environments)
    terraform output console_password | base64 -d | gpg --batch --no-tty --yes --passphrase=$GPG_PASSPHRASE --pinentry-mode loopback --decrypt --always-trust
    # or
    terraform output secret_access_key | base64 -d | gpg --batch --no-tty --yes --passphrase=$GPG_PASSPHRASE --pinentry-mode loopback --decrypt --always-trust
    ```


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-------:|:--------:|
| aws_default_region | The region required for AWS provider. | string | `""` | yes |
<!-- TODO -->


## Outputs

| Name | Description |
|------|-------------|
| this_iam_policy_id | The policy's ID |
<!-- TODO -->