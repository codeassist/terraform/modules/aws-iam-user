# Whether to create the resources ("false" prevents the module from creating any resources).
iam_user_module_enabled = false
# (Required) The user's name. The name must consist of upper and lowercase alphanumeric characters with no spaces.
# You can also include any of the following characters: =,.@-_
# User names are not distinguished by case.
iam_user_name = "test-1"
# Key-value mapping of tags for the IAM user.
iam_user_tags = {}
# Destroy even if it has non-Terraform-managed IAM access keys.
iam_user_force_destroy = false
# Either a base-64 encoded PGP public key, or a keybase username in the form keybase:username.
iam_user_secret_access_pgp_key = ""
# The access key status to apply.
  # Valid values are:
  #   * Active
  #   * Inactive.
iam_user_access_key_status = "Inactive"
# If set to "true" - enables ability to login into console
iam_user_password_enabled = false
# The length of the generated password on resource creation. Only applies on resource creation. Drift detection
# is not possible with this argument.
iam_user_password_length = 20
# Whether the user should be forced to reset the generated password on resource creation. Only applies
# on resource creation. Drift detection is not possible with this argument.
iam_user_password_reset_required = true
# (Required) Either a base-64 encoded PGP public key, or a keybase username in the form "keybase:username". Only applies
# on resource creation. Drift detection is not possible with this argument.
iam_user_password_pgp_key = ""
# The IAM policy in JSON format.
iam_user_policy_document = <<-JSON
  {
      "Version": "2012-10-17",
      "Statement": []
  }
  JSON
# The list of IAM policies ARNs need to be attached to the IAM user.
iam_user_policies_attachments = []
# The list of names of IAM groups which this user must be added to.
iam_user_groups = []
