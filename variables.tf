variable "iam_user_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # "false" prevents the module from creating any resources
  default = false
}

variable "iam_user_name" {
  description = "(Required) The user's name. The name must consist of upper and lowercase alphanumeric characters with no spaces."
  # You can also include any of the following characters: =,.@-_
  # User names are not distinguished by case.
  type = string
}
variable "iam_user_tags" {
  description = "Key-value mapping of tags for the IAM user."
  type        = map(string)
  default     = {}
}
variable "iam_user_force_destroy" {
  description = "Destroy even if it has non-Terraform-managed IAM access keys."
  type        = bool
  default     = false
}

variable "iam_user_access_key_status" {
  description = "The access key status to apply."
  type        = string
  # Valid values are:
  #   * Active
  #   * Inactive.
  default = "Inactive"
}
variable "iam_user_secret_access_pgp_key" {
  description = "Either a base-64 encoded PGP public key, or a keybase username in the form keybase:username."
  type        = string
  default     = ""
}

variable "iam_user_password_enabled" {
  description = "If set to true - enables ability to login into console"
  type        = bool
  default     = false
}
variable "iam_user_password_length" {
  # Only applies on resource creation. Drift detection is not possible with this argument.
  description = "The length of the generated password on resource creation."
  default     = 20
}
variable "iam_user_password_reset_required" {
  # Only applies on resource creation. Drift detection is not possible with this argument.
  description = "Whether the user should be forced to reset the generated password on resource creation."
  type        = bool
  default     = true
}
variable "iam_user_password_pgp_key" {
  # Only applies on resource creation. Drift detection is not possible with this argument.
  description = "(Required) Either a base-64 encoded PGP public key, or a keybase username in the form keybase:username."
  type        = string
  default     = ""
}

variable "iam_user_policy_document" {
  description = "The IAM policy in JSON format."
  type        = string
  default     = ""
}
variable "iam_user_policies_attachments" {
  description = "The list of IAM policies ARNs need to be attached to the IAM user."
  type        = list(string)
  default     = []
}
variable "iam_user_groups" {
  description = "The list of names of IAM groups which this user must be added to."
  type        = list(string)
  default     = []
}
